import PropType from 'prop-types';
function Header(props) {
    return (
        <header>
            <h1>Header {props.name}</h1>
        </header>
    );
}

Header.propType = {
    name: PropType.string
};

export default Header;