import PropType from 'prop-types';
function Photos(props) {
    return (
        <div 
        
        className="photos">
            {props.photos.map(data => (
                <img key={data.id} className="img" src={data.url} id={data.id} alt={data.title}/>
            ))}
        </div>
    );
}

Photos.propType = {
    name: PropType.array
  };

export default Photos;
