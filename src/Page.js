
import './App.css';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';

function Page() {
  return (
    <div className='page'>
      <Header name='title'/>
      <Main />
      <Footer name='title'/>
    </div>
  );
}

export default Page;