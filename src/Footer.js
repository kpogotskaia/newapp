import PropType from 'prop-types';
function Footer(props) {
    return (
        <footer>
            <h1>Footer {props.name}</h1>
        </footer>
    );
}

Footer.propType = {
    name: PropType.string
};

export default Footer;